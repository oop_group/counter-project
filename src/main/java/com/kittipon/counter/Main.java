/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kittipon.counter;

/**
 *
 * @author kitti
 */
public class Main {
    public static void main(String[] args) {
        Counter counter01 = new Counter();
        Counter counter02 = new Counter();
        Counter counter03 = counter01;
        counter01.increase();
        counter01.increase();
        counter01.increase();
        counter01.print();
        counter02.print();
        counter02.decrease();
        counter03.decrease();
        counter02.increase();
        counter01.decrease();
        System.out.println(counter01.counternumber);
        System.out.println("Print all counter");
        counter01.print();
        counter02.print();
        counter03.print();
    }
}
